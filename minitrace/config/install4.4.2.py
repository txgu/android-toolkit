#! /usr/bin/python

import os, sys, traceback, subprocess

DIR=os.path.abspath(os.path.dirname(__file__))

ADB=os.getenv('ADB', 'adb')

APE_ROOT='/sdcard/'

def run_cmd(*args):
    print('Run cmd: ' + (' '.join(*args)))
    subprocess.check_call(*args)


if __name__ == '__main__':
    try:
        run_cmd([ADB, '-d', 'shell', 'su', '-c', 'mount', '-o', 'rw,remount', '/system'])
        run_cmd([ADB, '-d', 'push', os.path.join(DIR, 'minitrace-config.jar'), APE_ROOT])
        run_cmd([ADB, '-d', 'shell', 'su', '-c', 'cp', '/sdcard/minitrace-config.jar', '/system/framework/'])
        run_cmd([ADB, '-d', 'shell', 'su', '-c', 'chmod', '644', '/system/framework/minitrace-config.jar'])
        run_cmd([ADB, '-d', 'shell', 'su', '-c', 'chown', 'root:root', '/system/framework/minitrace-config.jar'])


        run_cmd([ADB, '-d', 'push', os.path.join(DIR, 'minitrace'), APE_ROOT])
        run_cmd([ADB, '-d', 'shell', 'su', '-c', 'cp', '/sdcard/minitrace', '/system/bin/'])
        run_cmd([ADB, '-d', 'shell', 'su', '-c', 'chmod', '755', '/system/bin/minitrace'])
        run_cmd([ADB, '-d', 'shell', 'su', '-c', 'chown', 'root:shell', '/system/bin/minitrace'])
    except:
        traceback.print_exc()

